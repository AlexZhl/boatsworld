$(document).ready(function () {
    $('form').addClass('form-inline');
    $('select').addClass('form-control');
    $('select').css({ 'max-width': '30%', 'display': 'inline-block' });
    $('input').addClass('form-control');
    $('input').css({ 'max-width': '50%', 'display': 'inline-block' });
    $('button').addClass('btn btn-default');
});