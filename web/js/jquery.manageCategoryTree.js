( function( $ ) {
    $.fn.manageCategoryTree = function(options) {
        var domElement = this[0];
        options = $.extend( {
            datatype: 'json',
            url: 'path to controller action',
            data: {
                categoryTreeParent: 0,
                // categoryTreeChild: 1,
                categoryTreeLevel: 0,
            },
            domElement: domElement,
        }, options);

        $.ajax( options )
            .done(function (data) {
                categories = JSON.parse( data );
                startTree( options, categories );
                stopLoadingAnimation();
            });
        startLoadingAnimation();
    }
} ) ( jQuery );

function startTree( options, categories ) {
    var ulTree = $( '<ul/>', {
        class: 'list-group'
    } );
    $.each( categories, function ( index, category ) {
        ulTree.append(
            $( '<li/>', {
                class: 'list-group-item',
                categoryId: category.id,
                level: options.data.categoryTreeLevel
            } ).append(
                $( '<span/>', {
                    class: 'glyphicon glyphicon-chevron-right clickable',
                    click: function getChilds() {
                        var liElement = event.target.parentNode;
                        addTree(
                            options,
                            categories,
                            category.id,
                            parseInt( liElement.getAttribute('level') )+1,
                            liElement
                        );
                        $( this ).remove();
                    }
                }),

                $( '<span/>', {
                    class: 'not-clickable',
                    text: category.title,
                    click: function () {
                    }
                }),

                $( '<a/>', {
                    class: 'glyphicon glyphicon-plus-sign clickable',
                    click: function () {
                        var input = $( '#modal-add-input' )[0];
                        input.parentId = category.id;
                        input.level = ( parseInt(category.level) + 1 );

                        var addBtn = $( '#modal-add-btn' );
                        addBtn.click( function () {
                            if ( typeof input.value != 'undefined' ) {
                                this.href = options.actions.addSub.url + '?categoryTitle=' + input.value +
                                    '&parentId=' + input.parentId + '&level=' + input.level;
                                //this.click();
                            }
                        });

                        $( '#modal-add' ).modal( 'show' );
                    }
                }),
                $( '<a/>', {
                    class: 'glyphicon glyphicon-minus-sign clickable',
                    click: function () {
                        var input = $( '#modal-add-input' )[0];
                        input.parentId = category.id;
                        input.level = ( parseInt(category.level) + 1 );

                        var deleteBtn = $( '#modal-delete-btn' );
                        deleteBtn.click( function () {
                            this.href = options.actions.delete.url + '?id=' + category.id
                        });

                        $( '#modal-delete-confirmation' ).modal( 'show' );
                    }
                })
            )
        );
    })

    $( options.domElement ).append( ulTree );
    // Protects from creating ">" element
    if ( options.domElement.childElementCount > 1 ) {
        childsShowHide( options.domElement );
    }
}

function addTree( options, categories, parentId, level, element ) {
    options = $.extend(options, {
        domElement: element,
    });
    options.data = $.extend(options.data, {
        categoryTreeParent: parentId,
        categoryTreeLevel: level,
    });

    $( element ).manageCategoryTree( options );
}

function childsShowHide( element ) {
    $( element ).prepend(
        $( '<span/>', {
            class: 'glyphicon glyphicon-chevron-down clickable',
            click: function () {
                if (event.target.tagName != 'SPAN') {
                    return;
                }

                var li = event.target.parentNode; // получить родительский LI

                var childrenContainer = li.getElementsByTagName('ul')[0];

                if (!childrenContainer) return;

                if ( childrenContainer.hidden ) {
                    $( this ).removeClass('glyphicon-chevron-right');
                    $( this ).addClass('glyphicon-chevron-down');
                } else {
                    $( this ).removeClass('glyphicon-chevron-down');
                    $( this ).addClass('glyphicon-chevron-right');
                }
                childrenContainer.hidden = !childrenContainer.hidden;
            }
        })
    )
}

function startLoadingAnimation() {
    var imgObj = $( "#ajax-loading" );
    imgObj.show();
}
function stopLoadingAnimation() {
    $( "#ajax-loading" ).hide();
}