( function( $ ) {
    $.fn.fineGrid = function(options) {
        var domElement = this[0];
        options = $.extend( {
            datatype: 'json',
            url: 'getinfo.php',
            data: {
                pageNumber: 1,
                itemsPerPage: 10,
            },
            domElement: domElement,
        }, options);

        ajaxQuery( options );
    }
} ) ( jQuery );

function sortRefresh( options, pagination, criteria, newDirection ) {

    // this skipped if options.data.direction is undefined so it only works for >=2nd sort query
    if (options.data.direction == 'asc') {
        newDirection = 'desc'
    } else if (options.data.direction == 'desc') {
        newDirection = 'asc'
    }

    options.data = $.extend( options.data, {
        sort: criteria,
        direction: newDirection,
        page: options.data.pageNumber,
    });

    ajaxQuery( options );
}

function filterRefresh( options, pagination, field, value ) {

    options.data = $.extend( options.data, {
        pageNumber: 1,
        filterValue: value,
        filterField: field
    });

    ajaxQuery( options );
}

function buildTable ( options, pagination ) {

    var itemsData = pagination.items;
    var fields = options.data.fields;

    // Create new <table> with <thead> and <tbody>
    var newTable = $('<table/>', {
        class:'table table-striped'
    }).append(
        $('<thead/>'),
        $('<tbody/>')
    );

    // Fill <thead>
    var rowHead = $( '<tr/>' );
    $.each(fields, function( fieldName, fieldData ) {
        if (fieldData.sort.sortable == false) {
            rowHead.append(
                $('<th/>', {
                    text: (typeof fieldData.label == "undefined" ? fieldName : fieldData.label)
                })
            );
        } else {
            var tableHeadCell = $( '<th/>' ).appendTo(rowHead);
            var cellText = $('<a/>', {
                text: (typeof fieldData.label == "undefined" ? fieldName : fieldData.label) ,
                title: fieldName,
                class: 'sortable',
                click: function () {
                    sortRefresh( options, pagination, fieldData.sort.criteria, fieldData.sort.direction );
                },
            }).appendTo(tableHeadCell);

            if ( options.data.sort == fieldData.sort.criteria ) {
                if ( options.data.direction == 'asc' ) {
                    $( '<span/>', { class: 'glyphicon glyphicon-triangle-top'} ).appendTo(tableHeadCell);
                } else {
                    $( '<span/>', { class: 'glyphicon glyphicon-triangle-bottom'} ).appendTo(tableHeadCell);
                }
            }

        }
    });
    $("thead", newTable).append(rowHead);

    // fill <tbody>
    $.each( itemsData, function( index, item ) {
        // create row
        var rowData = $('<tr/>');

        // fill row
        $.each( item, function( fieldName, fieldValue ) {
            rowData.append(
                $('<td/>', {
                    text:fieldValue
                })
            );
        });

        $.each( options.data.actions, function ( index, action ) {
            rowData.append(
                $( '<td/>' ).append(
                    $( '<a/>', {
                        class: 'btn btn-xs btn-primary',
                        text: action.label,
                        href: action.link + '?id=' + item.id
                    })
                )
            );
        });

        // add created row to 'newTable'
        $("tbody",newTable).append(rowData);
    });

    return newTable;
}

function buildFilterForm( options, pagination ) {

    var selectOptionsArray = $( '<select/>', { id: 'filterField', class: 'form-control' });
    $.each( options.data.filter, function (index, filterFieldOption) {
        var eachOption = $( '<option/>', {
            value: filterFieldOption.filterField,
            text: filterFieldOption.filterFieldText
        });
        if (options.data.filterField == filterFieldOption.filterField) {
            eachOption[0].selected = 'selected';
        }
        eachOption.appendTo(selectOptionsArray);
    });

    var newFilter = $( '<div/>', { class: 'form-group form-inline' })
                        .append(
                            $( '<div/>', { class: 'input-group' } )
                                .append(
                                    selectOptionsArray,
                                    $( '<input/>', {
                                        class: 'form-control',
                                        value: options.data.filterValue,
                                        id: 'filterValue',
                                        placeholder: 'Type here',
                                        type: 'text'
                                    }),
                                    $( '<button/>', {
                                        text: 'Filter',
                                        class: 'btn btn-default',
                                        click: function () {
                                            filterRefresh(
                                                options,
                                                pagination,
                                                $( '#filterField' ).val(),
                                                $( '#filterValue' ).val()
                                            );
                                        }
                                    })
                                )
                        );
    return newFilter;
}

function goToPage( options, page ) {

    options.data = $.extend( options.data, {
        pageNumber: page,
    });

    ajaxQuery( options );
}

function buildGrid( options, pagination ) {

    var div = $( '<div/>' )
        .append(
            buildFilterForm( options, pagination ),
            buildTable( options, pagination ),
            buildPageLinks( options, pagination )
        );
    $( options.domElement ).html( div );
}

function buildPageLinks( options, pagination ) {
    var divNavigation = $( '<div/>', { class: 'navigation' } );

    var viewData = getPaginationData( options, pagination );
    if ( viewData.pageCount > 1 ) {
        var ulPagination = $( '<ul/>', { class: 'pagination' } );
        divNavigation.append(ulPagination);

        // Element '« Previous'
        if ( typeof viewData.previous != 'undefined' ) {
            ulPagination.append(
                $( '<li/>' ).append(
                    $( '<a/>', {
                        rel: 'prev',
                        text: '« Previous',
                        click: function () {
                            goToPage( options, viewData.current-1 );
                        }
                    } )
                )
            );
        } else {
            ulPagination.append(
                $( '<li/>', {
                    class: 'disabled'
                } ).append(
                    $( '<span/>', { text: '« Previous' } )
                )
            );
        }

        // First pages
        if ( viewData.startPage > 1 ) {
            var liPage1 = $( '<li/>' ).append(
                $( '<a/>', {
                    text: 1,
                    click: function () {
                        goToPage( options, 1 );
                    }
                } )
            );
            ulPagination.append(liPage1);

            if ( viewData.startPage == 3 ) {
                var liPage2 =
                    $( '<li/>' ).append(
                        $( '<a/>', {
                            text: 2,
                            click: function () {
                                goToPage( options, 2 );
                            }
                        } )
                    );
                ulPagination.append(liPage2);
            } else if ( viewData.startPage != 2 ) {
                var liDisabled =
                    $( '<li/>', { class: 'disabled'} )
                        .append(
                            $( '<span/>', { text: '...' } )
                        );
                ulPagination.append(liDisabled);
            }
        }// First pages

        // [1] [..] [6] [7] [8] [9] [..] [19] Pages view
        $.each( viewData.pagesInRange, function ( index, value ) {
            if ( value != viewData.current ) {
                ulPagination.append(
                    $( '<li/>' )
                        .append(
                            $( '<a/>', {
                                text: value,
                                click: function () {
                                    goToPage( options, value );
                                }
                            })
                        )
                );
            } else {
                ulPagination.append(
                    $( '<li/>', { class: 'active' } )
                        .append(
                            $( '<span/>', { text: value } )
                        )
                );
            }
        });// [1] [..] [6] [7] [8] [9] [..] [19] Pages view

        // Last pages
        if ( viewData.pageCount > viewData.endPage ) {
            if ( viewData.pageCount > viewData.endPage+1 ) {
                if ( viewData.pageCount > viewData.endPage+2 ) {
                    ulPagination.append(
                        $( '<li/>', { class: 'disabled' } )
                            .append(
                                $( '<span/>', { text: '...' } )
                            )
                    );
                } else {
                    ulPagination.append(
                        $( '<li/>' )
                            .append(
                                $( '<a/>', {
                                    text: viewData.pageCount-1,
                                    click: function () {
                                        goToPage( options, viewData.pageCount-1 );
                                    }
                                })
                            )
                    );
                }
            }
            ulPagination.append(
                $( '<li/>' )
                    .append(
                        $( '<a/>', {
                            text: viewData.pageCount,
                            click: function () {
                                goToPage( options, viewData.pageCount );
                            }
                        })
                    )
            );
        }// Last pages

        // Element 'Next »'
        if ( typeof viewData.next != 'undefined' ) {
            ulPagination.append(
                $( '<li/>' )
                    .append(
                        $( '<a/>', {
                            rel: 'prev',
                            text: 'Next »',
                            click: function () {
                                goToPage( options, viewData.current+1 );
                            }
                        } )
                    )
            );
        } else {
            ulPagination.append(
                $( '<li/>', {
                    class: 'disabled'
                } ).append(
                    $( '<span/>', { text: 'Next »' } )
                )
            );
        }// Element 'Next »'
    }

    return divNavigation;
}

function getPaginationData( options, pagination ) {
    var pageCount = Math.ceil( pagination.total_count / pagination.num_items_per_page );
    var current = pagination.current_page_number;
    var pageRange = pagination.page_range;
    if (pageCount < current) {
        current = pageCount;
    }
    if (pageRange > pageCount) {
        pageRange = pageCount;
    }

    var delta = Math.ceil(pageRange / 2);
    var pages;
    var offset;

    if (current - delta > pageCount - pageRange) {
        pages = newJsRange(pageCount - pageRange + 1, pageCount);
    } else {
        if (current - delta < 0) {
            delta = current;
        }

        offset = current - delta;
        pages = newJsRange(offset + 1, offset + pageRange);
    }

    var proximity = Math.floor(pageRange / 2);

    var startPage  = current - proximity;
    var endPage    = current + proximity;

    if (startPage < 1) {
        endPage = Math.min(endPage + (1 - startPage), pageCount);
        startPage = 1;
    }

    if (endPage > pageCount) {
        startPage = Math.max(startPage - (endPage - pageCount), 1);
        endPage = pageCount;
    }

    var viewData = {
        last              : pageCount,
        current           : current,
        numItemsPerPage   : pagination.num_items_per_page,
        first             : 1,
        pageCount         : pageCount,
        totalCount        : pagination.total_count,
        pageRange         : pageRange,
        startPage         : startPage,
        endPage           : endPage
    }

    if (current - 1 > 0) {
        viewData.previous = current - 1;
    }

    if (current + 1 <= pageCount) {
        viewData.next = current + 1;
    }

    viewData.pagesInRange = pages;
    viewData.firstPageInRange = Math.min(pages);
    viewData.lastPageInRange  = Math.max(pages);

    // if (typeof pagination.items !== 'undefined') {
    //     viewData['currentItemCount'] = $this->count();
    //     viewData['firstItemNumber'] = (($current - 1) * $this->numItemsPerPage) + 1;
    //     viewData['lastItemNumber'] = $viewData['firstItemNumber'] + $viewData['currentItemCount'] - 1;
    // }

    return viewData;
}

var newJsRange = function(start, end, step) {
    var range = [];
    var typeofStart = typeof start;
    var typeofEnd = typeof end;

    if (step === 0) {
        throw TypeError("Step cannot be zero.");
    }

    if (typeofStart == "undefined" || typeofEnd == "undefined") {
        throw TypeError("Must pass start and end arguments.");
    } else if (typeofStart != typeofEnd) {
        throw TypeError("Start and end arguments must be of same type.");
    }

    typeof step == "undefined" && (step = 1);

    if (end < start) {
        step = -step;
    }

    if (typeofStart == "number") {

        while (step > 0 ? end >= start : end <= start) {
            range.push(start);
            start += step;
        }

    } else if (typeofStart == "string") {

        if (start.length != 1 || end.length != 1) {
            throw TypeError("Only strings with one character are supported.");
        }

        start = start.charCodeAt(0);
        end = end.charCodeAt(0);

        while (step > 0 ? end >= start : end <= start) {
            range.push(String.fromCharCode(start));
            start += step;
        }

    } else {
        throw TypeError("Only string and number types are supported");
    }

    return range;

}


function ajaxQuery( options ) {
    $.ajax( options )
        .done(function (data) {
            stopLoadingAnimation();
            var pagination = JSON.parse( data );
            buildGrid( options, pagination );
        });
    startLoadingAnimation();
}

function startLoadingAnimation() {
    var imgObj = $( "#ajax-loading" );
    imgObj.show();
}

function stopLoadingAnimation() {
    $( "#ajax-loading" ).hide();
}