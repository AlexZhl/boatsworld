<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{
    /**
     * @Route("/register/", name="app.registration.register")
     */
    public function registerAction(Request $request)
    {
        // Redirect to homepage if user is already logged in
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app.home.index');
        }

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this
                ->get('form_handler.registration_form')
                ->handleRegistrationForm($user);
            return $this->redirectToRoute('app.catalog.catalog');
        }

        return $this->render('registration/register.html.twig', [
            'form' => $form->createView(), ]);
    }
}
