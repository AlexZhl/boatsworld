<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CatalogController extends Controller
{
    const PAGE_NUMBER = 1;
    const ITEMS_PER_PAGE = 8;

    /**
     * @Route("/catalog/", name="app.catalog.catalog")
     */
    public function catalogAction(Request $request)
    {
        $page = $request->query->getInt('page', self::PAGE_NUMBER);
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');
        $productsRepo = $this->getDoctrine()->getRepository(Product::class);

        $productsData = $productsRepo
            ->getProductsData(
                $paginator,
                $page,
                $itemsPerPage
            );
        $jsonCategories = $this->get('jms_serializer')->serialize($productsData['categories'], 'json');

        return $this->render('catalog/catalog.html.twig', [
            'pagination' => $productsData['pagination'],
            'jsonCategories' => $jsonCategories,
        ]);
    }

    /**
     * @Route("/catalog/{id}/details/", name="app.catalog.catalogDetails")
     */
    public function detailsAction($id)
    {
        $product = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->find($id);

        if (!$product) {
            $this->addFlash('error', 'Unknown error');
            return $this->redirectToRoute('app.catalog.catalog');
        }

        return $this->render('catalog/details.html.twig', [
            'product' => $product,
        ]);
    }
}
