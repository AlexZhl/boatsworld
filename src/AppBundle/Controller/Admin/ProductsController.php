<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductsController extends Controller
{
    const PAGE_NUMBER = 1;
    const ITEMS_PER_PAGE = 10;

    /**
     * @Route("/admin/products/", name="app.products.products")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->getInt('page', self::PAGE_NUMBER);
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');
        $productsRepo = $this->getDoctrine()->getRepository(Product::class);

        $productsData = $productsRepo->getProductsData($paginator, $page, $itemsPerPage);

        return $this->render('admin/products/products.html.twig', [
            'pagination' => $productsData['pagination'],
        ]);
    }

    /**
     * @Route("/admin/products/create/", name="app.products.create")
     */
    public function createProductAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formHandler = $this->get('form_handler.product_form');
            $formHandler->handleCreateForm($product);
            $this->addFlash('notice', ('Product ' . $product->getTitle() . ' created!'));
            return $this->redirectToRoute('app.products.products');
        }

        return $this->render('admin/products/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/products/{id}/edit/", name="app.products.edit")
     */
    public function editProductAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);
        $oldImageName = $product->getImageUrl();
        $product->setImageUrl(null);
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formHandler = $this->get('form_handler.product_form');
            $formHandler->handleEditForm($product, $oldImageName);
            $this->addFlash('notice', ('Product ' . $product->getTitle() . ' edited'));
            return $this->redirectToRoute('app.products.products');
        }

        return $this->render('admin/products/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/products/{id}/delete/", name="app.products.delete")
     */
    public function deleteProductAction($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if ($product) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
            $this->addFlash('notice', 'Product removed');
        } else {
            $this->addFlash('error', 'Unknown error');
        }

        return $this->redirectToRoute('app.products.products');
    }
}
