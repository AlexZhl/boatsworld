<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    const PAGE_NUMBER = 1;
    const ITEMS_PER_PAGE = 10;

    /**
     * @Route("/admin/", name="app.admin.admin")
     */
    public function adminAction(Request $request)
    {
        $page = $request->query->getInt('page', self::PAGE_NUMBER);
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);
        $paginator = $this->get('knp_paginator');
        $messagesRepo = $this->getDoctrine()->getRepository(Message::class);
        $messages = $paginator
            ->paginate($messagesRepo->getMessagesSelectQuery(), $page, $itemsPerPage);

        return $this->render('admin/admin.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/admin/message/{id}/", name="app.admin.detailedMessage")
     */
    public function detailedMessageAction($id)
    {
        $message = $this->getDoctrine()->getRepository('AppBundle:Message')->find($id);

        return $this->render('admin/detailedMessage.html.twig', [
            'message' => $message,
        ]);
    }

    /**
     * @Route("/admin/message/{id}/delete", name="app.admin.deleteMessage")
     */
    public function deleteMessage($id)
    {
        $message = $this->getDoctrine()->getRepository(Message::class)->find($id);
        if ($message) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($message);
            $em->flush();
            $this->addFlash('notice', 'Message removed successfully');
        } else {
            $this->addFlash('error', 'Unknown error');
        }

        return $this->redirectToRoute('app.admin.admin');
    }
}
