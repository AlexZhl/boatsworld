<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\UserEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    const PAGE_NUMBER = 1;

    /**
     * @Route("/admin/users/", name="app.users.users")
     */
    public function indexAction(Request $request)
    {
        return $this->render('admin/users/users.html.twig');
    }

    /**
     * @Route("/admin/users/ajax", name="app.users.getUsersAjax")
     */
    public function getUsersAjaxAction(Request $request)
    {
        $paginator  = $this->get('knp_paginator');
        $serializer = $this->get('jms_serializer');
        $itemsPerPage = $request->query->getInt('itemsPerPage', self::ITEMS_PER_PAGE);
        $pageNumber = $request->query->getInt('pageNumber', self::PAGE_NUMBER);

        $selectUsersQuery = $this->getDoctrine()->getRepository(User::class)->getSelectUsersQuery();
        $pagination = $paginator->paginate($selectUsersQuery, $pageNumber, $itemsPerPage);

        return new JsonResponse($serializer->serialize($pagination, 'json'));
    }

    /**
     * @Route("/admin/users/edit/", name="app.users.edit")
     */
    public function editAction(Request $request)
    {
        $id = $request->query->getInt('id');
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if (!$user) {
            $this->addFlash('error', ('Unknown error'));
            return $this->redirectToRoute('app.users.users');
        }
        $form = $this->createForm(UserEditType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', ('User ' . $user->getUsername() . ' edited'));
            return $this->redirectToRoute('app.users.users');
        }

        return $this->render('admin/users/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/users/delete/", name="app.users.delete")
     */
    public function deleteAction(Request $request)
    {
        $id = $request->query->getInt('id');
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash('notice', 'User removed');
        } else {
            $this->addFlash('error', 'Unknown error');
        }

        return $this->redirectToRoute('admin/users/users.html.twig');
    }
}
