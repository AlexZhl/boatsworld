<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends Controller
{
    /**
     * @Route("/admin/categories/", name="app.categories.categories")
     */
    public function categoriesAction(Request $request)
    {
        return $this->render('admin/categories/categories.html.twig');
    }

    /**
     * @Route("/admin/categories/ajax/", name="app.categories.getAjax")
     */
    public function getCategoriesAjax(Request $request)
    {
        $parentId = $request->query->getInt('categoryTreeParent', 0);
        $level = $request->query->getInt('categoryTreeLevel', 0);

        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepo->loadCategoryByAjax($parentId, $level);

        $serializer = $this->get('jms_serializer');

        return new JsonResponse($serializer->serialize($categories, 'json'));
    }

    /**
     * @Route("/admin/categories/create/", name="app.categories.create")
     */
    public function createCategory(Request $request)
    {
        $categoryTitle = $request->query->get('categoryTitle');
        $this
            ->getDoctrine()
            ->getRepository(Category::class)
            ->createCategory($categoryTitle);

        return $this->render('admin/categories/categories.html.twig');
    }

    /**
     * @Route("/admin/categories/addsub/", name="app.categories.addsub")
     */
    public function addSubCategory(Request $request)
    {
        $categoryTitle = $request->query->get('categoryTitle');
        $parentId = $request->query->get('parentId');
        $level = $request->query->get('level');
        $this
            ->getDoctrine()
            ->getRepository(Category::class)
            ->addSubCategory($categoryTitle, $parentId, $level);
        return $this->redirectToRoute('app.categories.categories');
    }

    /**
     * @Route("/admin/categories/delete/", name="app.categories.delete")
     */
    public function deleteCategory(Request $request)
    {
        $id = $request->query->getInt('id');

        if ($id) {
            $this->getDoctrine()->getRepository(Category::class)->deleteCategory($id);
            $this->addFlash('notice', 'Category with all sub-categories removed');
        } else {
            $this->addFlash('error', 'Unknown error');
        }

        return $this->redirectToRoute('app.categories.categories');
    }
}
