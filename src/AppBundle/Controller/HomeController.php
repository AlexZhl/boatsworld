<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
     * @Route("/", name="app.home.index")
     */
    public function indexAction()
    {
        return $this->render('home/index.html.twig', []);
    }

    /**
     * @Route("/about/", name="app.home.about")
     */
    public function aboutAction()
    {
        return $this->render('home/about.html.twig', []);
    }

    /**
     * @Route("/contact/", name="app.home.contact")
     */
    public function contactAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            $this->addFlash('notice', 'Thank You! Your Message Has Been Submitted Successfully.');

            return $this->redirectToRoute('app.home.contact');
        }

        return $this->render('home/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
