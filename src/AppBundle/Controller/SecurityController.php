<?php
namespace AppBundle\Controller;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\ForgotPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login/", name="app.security.login")
     */
    public function loginAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app.home.index');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername,
        ]);
    }

    /**
     * @Route("/forgot-password/", name="app.security.forgot")
     */
    public function forgotAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app.home.index');
        }

        $user = new User();
        $form = $this->createForm(ForgotPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formHandler = $this->get('form_handler.forgot_password_form');

            if ($formHandler->handleForgotForm($user->getEmail())) {
                $this->addFlash(
                    'notice',
                    'To reset your password, follow the instructions in the email we\'ve just sent you.'
                );
                return $this->redirectToRoute('app.security.forgot');
            } else {
                $this->addFlash('error', 'That email address doesn\'t match any user accounts. ');
                return $this->render('security/forgot.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
        }

        return $this->render('security/forgot.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/forgot-password/reset/{accessToken}", name="app.security.resetPassword")
     */
    public function resetPasswordAction($accessToken, Request $request)
    {
        $token = $this
            ->getDoctrine()
            ->getRepository(ResetPasswordToken::class)
            ->findOneBy([ 'accessToken' => $accessToken ]);
        if (!$token || $token->isExpired()) {
            $this->addFlash('error', 'Invalid link');
            return $this->redirectToRoute('app.home.index');
        }

        $form = $this->createForm(ChangePasswordType::class, $token->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formHandler = $this->get('form_handler.change_password_form');
            if ($formHandler->handleChangePasswordForm($token)) {
                $this->addFlash('notice', 'Password recovered successfully! You can now log in.');
                return $this->redirectToRoute('app.home.index');
            }
        }

        return $this->render('security/changePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
