<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('sku', TextType::class)
            ->add('imageUrl', FileType::class, [ 'required' => false ])
            ->add('category', EntityType::class, [
                'class' => 'AppBundle:Category',
                'choice_label' => 'title',
            ])
            ->add('linkedProduct1', EntityType::class, [
                'class' => 'AppBundle:Product',
                'choice_label' => 'title',
                'required' => false,
                'empty_data' => null
            ])
            ->add('linkedProduct2', EntityType::class, [
                'class' => 'AppBundle:Product',
                'choice_label' => 'title',
                'required' => false,
                'empty_data' => null
            ])
            ->add('linkedProduct3', EntityType::class, [
                'class' => 'AppBundle:Product',
                'choice_label' => 'title',
                'required' => false,
                'empty_data' => null
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Product'
        ]);
    }
}
