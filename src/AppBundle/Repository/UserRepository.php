<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use \Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\Paginator;

class UserRepository extends EntityRepository
{
    public function getSelectUsersQuery()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u.id, u.username, u.email, u.isActive, r.title')
            ->from('AppBundle:User', 'u')
            ->join('u.role', 'r');

        return $qb->getQuery();
    }
}
