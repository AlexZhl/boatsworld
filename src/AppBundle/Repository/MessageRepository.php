<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Message;
use Doctrine\Orm\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function getMessagesSelectQuery()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm');

        return $qb->getQuery();
    }
}
