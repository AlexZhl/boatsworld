<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use \Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\Paginator;

class ProductRepository extends EntityRepository
{
    public function getProductsData(Paginator $paginator, $page, $itemsPerPage)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('p')
            ->from(Product::class, 'p')
            ->join(Category::class, 'c', 'WITH', 'p.category = c.id');
        $query = $qb->getQuery();

        $pagination = $paginator
            ->paginate($query, $page, $itemsPerPage);

        $categories = $this->_em->getRepository(Category::class)->getFullCategoriesTree();

        return [
            'pagination' => $pagination,
            'categories' => $categories,
        ];
    }

    public function replaceCategoryIfExists(Category $category, $newCategory)
    {
        $products = $this->findBy([ 'category' => $category ]);
        foreach ($products as $product) {
            $product->setCategory($newCategory);
        }
        $this->_em->flush();
    }
}
