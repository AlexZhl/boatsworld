<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CategoriesTree;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function loadCategoryByAjax($parent, $level)
    {
        $qb = $this->_em->createQueryBuilder();

        if ($parent == 0) {
            $qb
                ->select('c.id, c.title, c.level')
                ->from(Category::class, 'c')
                ->where('c.level = 0');
        } else {
            $qb
                ->select('c.id, c.title, c.level')
                ->from('AppBundle:Category', 'c')
                ->join('AppBundle:CategoriesTree', 't', 'WITH', 'c.id = t.child')
                ->where('t.parent = ?1')
                ->andWhere('c.level = ?2');
            $qb->setParameters([
                1 => $parent,
                2 => $level,
            ]);
        }
        $categories = $qb->getQuery()->getResult();

        return $categories;
    }

    public function createCategory($categoryTitle)
    {
        $em = $this->_em;

        $newCategory = new Category();
        $newCategory->setTitle($categoryTitle);
        $newCategory->setLevel(0);

        $newCategoriesTree = new CategoriesTree();
        $newCategoriesTree->setChild($newCategory);

        $em->persist($newCategoriesTree);
        $em->persist($newCategory);
        $em->flush([ $newCategory, $newCategoriesTree ]);
    }

    public function addSubCategory($categoryTitle, $parentId, $level)
    {
        $em = $this->_em;
        $repositoryCatTree = $em->getRepository(CategoriesTree::class);

        $newCategory = new Category();
        $newCategory->setTitle($categoryTitle);
        $newCategory->setLevel($level);

        $em->persist($newCategory);
        $em->flush($newCategory);

        $parent = $this->find($parentId);

        for ($i = $level; $i > 0; $i--) {
            $newCategoriesTree = new CategoriesTree();
            $newCategoriesTree->setParent($parent);
            $newCategoriesTree->setChild($newCategory);

            $em->persist($newCategoriesTree);
            $em->flush($newCategoriesTree);

            if ($i > 1) {
                $parent = $repositoryCatTree->getCategoryParent($parent, $parent->getLevel()-1);
            }
        }
    }

    public function deleteCategory($id)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $category = $this->find($id);
        $productsRepo = $em->getRepository(Product::class);
        $parentCategory = $em
            ->getRepository(CategoriesTree::class)
            ->getCategoryParent($id, $category->getLevel()-1);
        if ($category->getLevel() == 0) {
            $parentCategory = $this->findOneBy([]);
        }

        // Get all childs of our Category
        $qb
            ->select('c')
            ->from(Category::class, 'c')
            ->join(CategoriesTree::class, 't', 'WITH', 'c.id = t.child')
            ->where('t.parent = ?1');
        $qb->setParameter(1, $id);
        $childs = $qb->getQuery()->getResult();

        // Delete all records, which tells that Childs is someones childs
        foreach ($childs as $child) {
            $qb
                ->delete(CategoriesTree::class, 't')
                ->where('t.child = ?1');
            $qb->setParameter(1, $child->getId());
            $deleted = $qb->getQuery()->getResult();
        }

        // Delete childs
        foreach ($childs as $child) {
            $productsRepo->replaceCategoryIfExists($child, $parentCategory);
            $em->remove($child);
        }
        $this->_em->flush();

        // Delete all records, that tells our Category is someones child
        $qb
            ->delete(CategoriesTree::class, 't')
            ->where('t.child = ?1');
        $qb->setParameter(1, $id);
        $deleted = $qb->getQuery()->getResult();

        // Delete our category
        $productsRepo->replaceCategoryIfExists($category, $parentCategory);
        $qb
            ->delete(Category::class, 'c')
            ->where('c.id = ?1');
        $qb->setParameter(1, $id);
        $deleted = $qb->getQuery()->getResult();
    }



    public function getFullCategoriesTree()
    {
        $levelZero = $this->loadCategoryByAjax(0, 0);

        $tree = [ ];

        foreach ($levelZero as $category) {
            $category[ 'childs' ] = $this->getChildsRecursivly($category);
            array_push($tree, $category);
        }

        return $tree;
    }

    public function getChildsRecursivly($category)
    {
        $branch = [ ];

        $childs = $this->loadCategoryByAjax($category['id'], $category['level']+1);
        foreach ($childs as $child) {
            $child[ 'childs' ] = $this->getChildsRecursivly($child);
            array_push($branch, $child);
        }

        return $branch;
    }

    public function getChildCategory($parent, $level)
    {
        $qb = $this->_em->createQueryBuilder();

        if ($parent == 0) {
            $qb
                ->select('c')
                ->from(Category::class, 'c')
                ->where('c.level = 0');
        } else {
            $qb
                ->select('c')
                ->from('AppBundle:Category', 'c')
                ->join('AppBundle:CategoriesTree', 't', 'WITH', 'c.id = t.child')
                ->where('t.parent = ?1')
                ->andWhere('c.level = ?2');
            $qb->setParameters([
                1 => $parent,
                2 => $level,
            ]);
        }
        $categories = $qb->getQuery()->getResult();

        return $categories;
    }
}
