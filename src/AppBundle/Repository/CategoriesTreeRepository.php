<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoriesTreeRepository extends EntityRepository
{
    public function getCategoryParent($id, $level)
    {
        if ($level < 0) {
            return null;
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('c')
            ->from('AppBundle:Category', 'c')
            ->join('AppBundle:CategoriesTree', 't', 'WITH', 'c.id = t.parent')
            ->where('t.child = ?1')
            ->andWhere('c.level = ?2');
        $qb->setParameters([
            1 => $id,
            2 => $level,
        ]);
        $result = $qb->getQuery()->getSingleResult();

        return $result;
    }
}
