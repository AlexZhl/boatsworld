<?php

namespace AppBundle\Repository;

use AppBundle\AppBundle;
use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class ResetPasswordTokenRepository extends EntityRepository
{
    public function removePreviousTokens(ResetPasswordToken $token)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->delete(ResetPasswordToken::class, 't')
            ->where('t.user = ?1');
        $qb->setParameter(1, $token->getUser());
        $deletedCount = $qb->getQuery()->getResult();
    }
}
