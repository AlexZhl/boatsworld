<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CategoriesTree;
use AppBundle\Entity\Category;
use AppBundle\Entity\Message;
use AppBundle\Entity\Product;
use AppBundle\Entity\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // TABLE roles
        $roleUser = new Role();
        $roleUser->setTitle('ROLE_USER');
        $manager->persist($roleUser);
        $roleAdmin = new Role();
        $roleAdmin->setTitle('ROLE_ADMIN');
        $manager->persist($roleAdmin);
        $roleSuperAdmin = new Role();
        $roleSuperAdmin->setTitle('ROLE_SUPER_ADMIN');
        $manager->persist($roleSuperAdmin);
        $manager->flush();


        // TABLE Users
        $rowsCount = 10;
        $encoder = $this->container->get('security.password_encoder');

        $userAlex = new User();
        $userAlex->setUsername('Alex');
        $userAlex->setEmail('Alex@mail.com');
        $password = $encoder->encodePassword($userAlex, '123');
        $userAlex->setPassword($password);
        $userAlex->setRole($roleSuperAdmin);
        $manager->persist($userAlex);

        for ($i = 1; $i <= $rowsCount; $i++) {
            $newUser = new User();
            $newUser->setUsername('User' . $i);
            $newUser->setEmail('user' . $i . '@mail.com');
            $password = $encoder->encodePassword($newUser, 'hello');
            $newUser->setPassword($password);
            $newUser->setRole($roleUser);
            $manager->persist($newUser);
            echo '.';
        }
        $manager->flush();


        // TABLE messages
        $rowsCount = 23;
        for ($i = 1; $i <= $rowsCount; $i++) {
            $newMessage = new Message();
            $newMessage->setFirstname('firstname' . $i);
            $newMessage->setLastname('lastname' . $i);
            $newMessage->setEmail('test_email' . $i . '@mail.com');
            $newMessage->setPhone('1' . $i . '-45' . $i);
            $newMessage->setMessage('Lorem ipsum dolor sit amet, consectetur adipisicing 
            elit. Explicabo itaque modi omnis quod repudiandae sapiente sint voluptate? 
            Adipisci architecto asperiores assumenda 
            dolore, dolorem ducimus explicabo impedit nisi quod soluta temporibus.
            ');
            $manager->persist($newMessage);
            echo '.';
        }
        $manager->flush();
    }
}
