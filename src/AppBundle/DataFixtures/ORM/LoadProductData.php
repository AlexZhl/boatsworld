<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CategoriesTree;
use AppBundle\Entity\Category;
use AppBundle\Entity\Message;
use AppBundle\Entity\Product;
use AppBundle\Entity\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadProductData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // TABLE categories
        $catYachts = new Category();
        $catYachts->setTitle('Yachts');
        $catYachts->setLevel(0);
        $manager->persist($catYachts);

        $catMegaYachts = new Category();
        $catMegaYachts->setTitle('Mega Yachts');
        $catMegaYachts->setLevel(0);
        $manager->persist($catMegaYachts);

        $catBoats = new Category();
        $catBoats->setTitle('Boats');
        $catBoats->setLevel(0);
        $manager->persist($catBoats);

        // TABLE categories_tree
        $catYachtsTree = new CategoriesTree();
        $catYachtsTree->setChild($catYachts);
        $manager->persist($catYachtsTree);

        $catMegaYachtsTree = new CategoriesTree();
        $catMegaYachtsTree->setChild($catMegaYachts);
        $manager->persist($catMegaYachtsTree);

        $catBoatsTree = new CategoriesTree();
        $catBoatsTree->setChild($catBoats);
        $manager->persist($catBoatsTree);


        // TABLE products
        $pr1 = new Product();
        $pr1->setTitle('Princess 40 M (131\')');
        $pr1->setSku('prnc-40m-131');
        $pr1->setCategory($catMegaYachts);
        $pr1->setDescription('The M Class long range cruising yachts herald a bold new era for Princess, the pinnacle of over 45 years of innovation and engineering excellence.

This new range of technically advanced yachts has already stamped the Princess marque firmly on the 100 ft-plus market.

The flagship Princess 40M is a sublime expanse of interior and exterior living space which can comfortably accommodate up to twelve guests and seven crew in complete privacy. Constructed to world-class standards and with enviable presence, she delivers class-leading levels of luxury and build detailing. She also offers enormous scope for customization, enabling owners to create their ultimate yacht.

Sleek looks disguise the tri-deck yacht’s enormous interior which offers considerably more volume than many rivals of the same length, whilst great attention has been paid to achieving the highest recognized standards for quietness and comfort. Drop down balconies transform the main saloon into a spectacular area for entertaining and relaxing with a formal dining area enjoying extensive views through panoramic windows.

The magnificent owners suite takes pride of place forward of the main deck and is exceptionally spacious with a palatial bathroom which can be specified as individual ‘his and hers’ if required. It also features expansive walk-in wardrobes and an ante-room which owners can specify to suit their requirements. Space below deck is exceptional and can accommodate up to five additional staterooms with ease.

Attention to detail, and to guest comfort, are evident throughout, in everything right down to the under floor heating in all guest bathrooms. A grand central staircase links each deck from the sundeck to the lower accommodation and the crew can circulate around the boat using their own discrete stairway between decks.

Superb sea-keeping is augmented with an impressive cruising range, achieved through engineered weight reduction and the latest infused molding technology.');
        $pr1->setImageUrl('95e1de302bc145e092bc441e7eee028f.jpeg');
        $manager->persist($pr1);

        $pr2 = new Product();
        $pr2->setTitle('Princess 82 Motor Yacht');
        $pr2->setSku('prnc-82');
        $pr2->setCategory($catYachts);
        $pr2->setDescription('Elegant, powerful and incredibly spacious the new Princess 82 Motor Yacht epitomizes everything that is Princess from her sleek, dynamic exterior to her impeccably finished interior. Her uniquely designed deep-V hull is resin infused to ensure a highly capable and efficient performance that is both assured and rewarding.

Extended window lines enhance her contemporary exterior styling to provide a profile that is sporting and sophisticated and provide exceptional views from the main deck saloon. The exterior space on board is plentiful with a large cockpit complete with teak table for alfresco dining, an intimate foredeck seating and sunbathing area for a more private escape and an incredible flybridge with additional dining, seating and sunbathing areas all serviced from a dedicated wet bar.

Her interior is light, sophisticated and beautifully detailed with an intelligent use of space throughout. The main deck saloon incorporates relaxed low level furnishings and a formal dining area which are separated from the wheelhouse and highly specified galley to ensure guest privacy, particularly important for owners with crew or chartering intentions.

The guest accommodation comprises of four beautifully appointed en-suite cabins. A full beam owner’s suite and forward VIP are complemented by a twin cabin to port and a further double stateroom to starboard. Crew are accommodated in a twin en-suite cabin aft of the machinery space together with a utility area which can be specified as a second twin crew cabin if required.');
        $pr2->setImageUrl('24023ae2eba1372dd4a11358590a102d.jpeg');
        $manager->persist($pr2);

        $pr3 = new Product();
        $pr3->setTitle('Princess V48 Open');
        $pr3->setSku('prnc-v48-open');
        $pr3->setCategory($catBoats);
        $pr3->setDescription('The new Princess V48 Open has an open design with a full-length cockpit and sunroof.  The boat will be built with twin Volvo IPS600 at 435hp.

Innovative flexibility continues into the beautiful lower deck salon which can be either an entertaining space or converted to provide for additional berths.  There is an added option of specifying this area with an inviting third stateroom.

The master stateroom is a beautifully appointed sanctuary while the forward guest stateroom enjoys the option of scissor berths and oval ports.');
        $pr3->setImageUrl('7ccd6c24c57bd60facb15935270e48ce.jpeg');
        $manager->persist($pr3);

        $pr4 = new Product();
        $pr4->setTitle('Cruisers 380 Express');
        $pr4->setSku('crsr-380');
        $pr4->setCategory($catYachts);
        $pr4->setDescription('The 380 Express cockpit design takes full advantage of the 380\'s large beam; with an enormous U-shaped lounge that cradles you in comfort and luxury; soft--yet supportive--multi-density foam seating covered in supple premium vinyl is accented with color-coordinated French stitching that surrounds you in elegance.  The wet bar is adorned with solid-surface countertops that accent the stainless steel pedestal sink.  Optional seating allows for a power rear fold-down sun lounge and chaise.  Additional options include choice of a cockpit refrigerator or ice maker, electric grill, and air conditioning.

The 380 helm features rich wood grains, metallic ornamentation, stainless detailing, and a hand-stitched leather steering wheel and accent panels.  The instrument panels features the largest screen available in this class of yacht.  Full instrumentation flanks the center screen, with every switch in reach.  The portside companion lounge is equally impressive, with its ergonomically designed floating headrest and comfortable chaise lounge.

The impressively large and stylish salon takes full advantage of its placement in the 380\'s mid-section with the addition of six large vertical ports.  With features perfect for entertaining such as an optional sofa sleeper, as well as screened portholes to let in the breeze, you and your guests can reconnect with the outdoors without even stepping outside.

The spacious galley features attractive simulated teak flooring and granite-like solid surface countertops, with an elegant beverage/glass cabinet, microwave, two-burner electric stove, and a large refrigerator/freezer.  A LCD TV and DVD entertainment center completes your on-board comfort.

The master features a walk-around queen-size island berth not found in yachts of this category, along with a large hanging locker and numerous cabinets for extra storage.  A unique "tucked away" privacy curtain is standard, or select the optional full bulkhead and door.

Aft, the cozy, yet spacious berth can be used for guest sleeping accommodations or extra seating with the stowed table and its own optional TV/DVD combo.

An optional second stateroom allows you to invite guests to stay through the night and enjoy the magnificent sunsets and sunrises.  The enclosed second stateroom features two separate bunks and a bulkhead for additional privacy.

The aft head includes a separate shower stall and features a stately countertop with Venetian-style tile backsplash.');
        $pr4->setImageUrl('44c9df45eb32dd21d6b4b453b2931e91.jpeg');
        $manager->persist($pr4);

        $pr5 = new Product();
        $pr5->setTitle('41\' Back Cove Flybridge');
        $pr5->setSku('bk-cv-flbrdg-41');
        $pr5->setCategory($catBoats);
        $pr5->setDescription('The Back Cove 41 is the largest vessel to-date from Back Cove.  She is built with a single Cummins 600 hp diesel engine, bow and stern thrusters, generator, reverse cycle heat and air conditioning, all standard.  

A center transom door leads from a generously sized swim platform to an expansive and comfortable cockpit with corners seats and storage to each side.  The genset and additional storage are accessed through the aft cockpit hatch while the forward hatch provides access to the engine room.

The interior layout provides two private queen-berth staterooms each with their own head; the master head contains a large shower stall with glass enclosure and the guest head has a large mirror and vessel sink.  The central galley location serves the open air cockpit as easily as the salon.  The salon has large opening windows and a bi-fold door.  The galley has two stainless steel refrigerator drawers and freezer mounted below the helm seat.  Unique features will include a side deck door as well as a 50 amp shorepower system with cord reel. 

Storage is also abundant aboard this 41 Back Cove.  In the master stateroom, there are two overhead lockers and two shelves located port and starboard, a port side locker/bureau with maple dovetail drawers and shelving, a starboard side cedar-lined hanging locker and additional space for bulk storage underneath the queen-size island berth.  The master head will have storage underneath the sink and locker directly outboard with a spacious shower and a shelf to store sundries.  The guest berth will be equipped with an outboard shelf, a bureau with drawer and drawer storage below the berth.  Below the companionway staircase, located in the cabin sole is a large fiberglass bin. 

In the galley and located in the space between the fridge and cooktop storage is provided by a 30 inch wide by 18 inch deep drawer, an 18 inch drawer under the aft dinette seat and a 16 inch deep drawer under the forward dinette seat.

There is an large open storage space in the area below the helm station sole that is roughly 40 cubic feet and the cockpit offers the same storage in the aft seats as is found in the Back Cove 37.  ');
        $pr5->setImageUrl('ae0bc7fc7dc4cfc7a4d2ca53681736ed.jpeg');
        $manager->persist($pr5);

        $rowsCount = 30;
        for ($i = 1; $i <= $rowsCount; $i++) {
            $newProduct = new Product();
            $newProduct->setTitle('Product' . $i);
            $newProduct->setDescription('description');
            $newProduct->setSku('prod-' . $i);
            $newProduct->setCategory($catBoats);

            $manager->persist($newProduct);
            echo '.';
        }
        $manager->flush();
    }
}
