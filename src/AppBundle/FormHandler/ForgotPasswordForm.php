<?php
namespace AppBundle\FormHandler;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\TwigBundle\TwigEngine;

class ForgotPasswordForm
{
    protected $doctrine;
    protected $em;
    protected $mailer;
    protected $mailerUser;
    protected $templating;
    protected $secret;

    public function __construct(
        Registry $doctrine,
        \Swift_Mailer $mailer,
        $mailer_user,
        TwigEngine $templating,
        $secret
    ) {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->mailer = $mailer;
        $this->mailerUser = $mailer_user;
        $this->templating = $templating;
        $this->secret = $secret;
    }

    public function handleForgotForm($email)
    {
        $user = $this
            ->doctrine
            ->getRepository(User::class)
            ->findOneBy([ 'email' => $email]);
        if (!$user) {
            return false;
        }

        $token = new ResetPasswordToken();
        $token->setUser($user);
        $token->setAccessToken(md5(($this->secret . time())));
        $this
            ->doctrine
            ->getRepository(ResetPasswordToken::class)
            ->removePreviousTokens($token);
        $this->em->persist($token);
        $this->em->flush();

        $this->sendPasswordRecoveryMail($token, $user->getEmail());
        return true;
    }

    public function sendPasswordRecoveryMail(ResetPasswordToken $token, $email)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Password recovery')
            ->setFrom($this->mailerUser, 'BoatsWorld')
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/passwordRecovery.html.twig', [
                    'token' => $token
                ]),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
