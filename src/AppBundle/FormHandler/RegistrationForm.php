<?php
namespace AppBundle\FormHandler;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class RegistrationForm
{
    protected $doctrine;
    protected $encoder;

    public function __construct(Registry $doctrine, UserPasswordEncoder $encoder)
    {
        $this->doctrine = $doctrine;
        $this->encoder = $encoder;
    }

    public function handleRegistrationForm($user)
    {
        $password = $this
            ->encoder
            ->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $roleUser = $this
            ->doctrine
            ->getRepository('AppBundle:Role')
            ->findOneBy([ 'title' => 'ROLE_USER' ]);
        $user->setRole($roleUser);
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
    }
}
