<?php
namespace AppBundle\FormHandler;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManager;

class ProductForm
{
    protected $em;
    protected $images_directory;

    public function __construct(EntityManager $em, $images_directory)
    {
        $this->em = $em;
        $this->images_directory = $images_directory;
    }

    public function handleCreateForm(Product $product)
    {
        $file = $product->getImageUrl();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file
            ->move(
                $this->images_directory,
                $fileName
            );
        $product->setImageUrl($fileName);
        $this->em->persist($product);
        $this->em->flush($product);
    }

    public function handleEditForm(Product $product, $oldImageName)
    {
        $file = $product->getImageUrl();
        if ($file instanceof UploadedFile) {
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file
                ->move(
                    $this->images_directory,
                    $fileName
                );
            $product->setImageUrl($fileName);
        } else {
            $product->setImageUrl($oldImageName);
        }
        $this->em->persist($product);
        $this->em->flush($product);
    }
}
