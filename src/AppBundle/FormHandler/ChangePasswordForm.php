<?php
namespace AppBundle\FormHandler;

use AppBundle\Entity\ResetPasswordToken;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class ChangePasswordForm
{
    protected $doctrine;
    protected $em;
    protected $encoder;

    public function __construct(Registry $doctrine, UserPasswordEncoder $encoder)
    {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->encoder = $encoder;
    }

    public function handleChangePasswordForm(ResetPasswordToken $token)
    {
        $user = $token->getUser();
        $password = $this
            ->encoder
            ->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $this->em->remove($token);
        $this->em->flush();
        return true;
    }
}
